<?php

namespace App\Models;

use Arr;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;
use Venturecraft\Revisionable\RevisionableTrait;

/**
 * @property int             user_id
 * @property int             id
 * @property ShippingService service
 * @property string          service_name
 * @property string          shipping_service_name
 * @property string          tracking_number
 */
class Package extends Model
{
    use SoftDeletes, RevisionableTrait;

    protected $revisionEnabled = true;
    protected $revisionCleanup = true; //Remove old revisions (works only when used with $historyLimit)
    protected $historyLimit    = 500;  //Maintain a maximum of 500 changes at any point of time, while cleaning up old revisions.

    protected $keepRevisionOf = [
        'ship_date',
        'estimated_delivery_date',
        'delivered_date',
        'last_status',
    ];

    protected $fillable = [
        'item',
        'description',
        'shipping_service_id',
        'tracking_number',
        'ship_date',
        'qty',
        'estimated_delivery_date',
        'delivered_date',
        'last_status',
    ];

    protected $dates = [
        'ship_date',
        'estimated_delivery_date',
        'delivered_date',
    ];

    public function getShippingServiceNameAttribute()
    {
        return Arr::get($this->service, 'label', '');
    }

    public function service(): BelongsTo
    {
        return $this->belongsTo(ShippingService::class, 'shipping_service_id');
    }


    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    public function files()
    {
        return $this->morphMany(File::class, 'fileable');
    }
}
