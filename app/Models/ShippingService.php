<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class ShippingService
 * @property string slug
 * @property string label
 * @property string homepage_url
 */
class ShippingService extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'slug',
        'label',
        'homepage_url',
    ];
}
