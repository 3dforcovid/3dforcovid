<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphTo;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property bool   is_profile
 * @property bool   is_package
 * @property string storage_name
 */
class File extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'file_name',
        'md5_sum',
        'meta_data',
        'is_profile',
        'is_package',
        'hash_name',
    ];

    protected $casts = [
        'is_profile' => 'boolean',
        'is_package' => 'boolean',
        'meta_data'  => 'json',
    ];

    /**
     * reverse morph relationship
     * @return MorphTo
     */
    public function fileable()
    {
        return $this->morphTo();
    }

    public function getStorageNameAttribute()
    {
        switch (true) {
            case $this->is_profile:
                return 'profile_images';
            case $this->is_package:
                return 'package_images';
        }

        return null;
    }

    public function getUrlAttribute()
    {
        $disk = $this->storage_name;

        return route('file.show', [$disk, $this->hash_name]);
    }
}
