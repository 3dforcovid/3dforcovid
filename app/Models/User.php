<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\MorphOne;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

/**
 * @property string            name
 * @property string            first_name
 * @property string            last_name
 * @property File|MorphOne     profile_image
 * @property Equipment|HasMany equipment
 * @property int               id
 */
class User extends Authenticatable
{
    use Notifiable, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'is_admin',
        'first_name',
        'last_name',
        'email',
        'password',
    ];

    protected $appends = ['name'];

    protected $cast = [
        'is_admin' => 'boolean',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function getNameAttribute(): string
    {
        return sprintf('%s %s', $this->first_name, $this->last_name);
    }

    public function getSmProfileImageUrlAttribute()
    {
        if (!$this->profile_image) {
            return route('file.show', ['profile_images', 'undefined']) . '/50';
        }

        return "{$this->profile_image->url}/50";
    }

    public function getProfileImageUrlAttribute()
    {
        if (!$this->profile_image) {
            return route('file.show', ['profile_images', 'undefined']);
        }

        return "{$this->profile_image->url}/200";
    }

    public function equipment(): HasMany
    {
        return $this->hasMany(Equipment::class);
    }

    public function social_media(): HasMany
    {
        return $this->hasMany(SocialMedia::class);
    }

    public function packages(): HasMany
    {
        return $this->hasMany(Package::class);
    }

    public function profile_image()
    {
        return $this->morphOne(File::class, 'fileable');
    }
}
