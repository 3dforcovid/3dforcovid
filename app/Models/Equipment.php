<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Equipment
 * @property Option brand
 * @property Option model
 * @package App\Models
 */
class Equipment extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'description',
        'brand_id',
        'model_id',
    ];

    public function getBrandNameAttribute()
    {
        return $this->brand->label ?: '';
    }

    public function getModelNameAttribute()
    {
        return $this->model->label ?: '';
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function brand()
    {
        return $this->hasOne(Option::class, 'id', 'brand_id');
    }

    public function model()
    {
        return $this->hasOne(Option::class, 'id', 'model_id');
    }
}
