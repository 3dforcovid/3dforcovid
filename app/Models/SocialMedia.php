<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @property SocialMediaService service
 */
class SocialMedia extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'user_id',
        'social_media_service_id',
        'url',
    ];

    public function service(): BelongsTo
    {
        return $this->belongsTo(SocialMediaService::class, 'social_media_service_id');
    }

    public function getNameAttribute()
    {
        return $this->service->label ?: '';
    }
}
