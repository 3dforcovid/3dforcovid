<?php

namespace App\Policies;

use App\Models\Package;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class PackagePolicy
{
    use HandlesAuthorization;


    /**
     * Determine whether the user can view any packages.
     *
     * @param User $user
     * @return mixed
     */
    public function viewAny(User $user)
    {
        return auth()->user()->is_admin || auth()->id() == $user->id;
    }

    /**
     * Determine whether the user can view the package.
     *
     * @param User    $user
     * @param Package $package
     * @return mixed
     */
    public function view(User $user, Package $package)
    {
        return auth()->user()->is_admin || $user->id == $package->user_id;
    }

    /**
     * Determine whether the user can create packages.
     *
     * @param User $user
     * @return mixed
     */
    public function create(User $user)
    {
        return true;
    }

    /**
     * Determine whether the user can update the package.
     *
     * @param User    $user
     * @param Package $package
     * @return mixed
     */
    public function update(User $user, Package $package)
    {
        return auth()->user()->is_admin || $user->id == $package->user_id;
    }

    /**
     * Determine whether the user can delete the package.
     *
     * @param User    $user
     * @param Package $package
     * @return mixed
     */
    public function delete(User $user, Package $package)
    {
        return auth()->user()->is_admin || $user->id == $package->user_id;
    }

    /**
     * Determine whether the user can restore the package.
     *
     * @param User    $user
     * @param Package $package
     * @return mixed
     */
    public function restore(User $user, Package $package)
    {
        return auth()->user()->is_admin;
    }

    /**
     * Determine whether the user can permanently delete the package.
     *
     * @param User    $user
     * @param Package $package
     * @return mixed
     */
    public function forceDelete(User $user, Package $package)
    {
        return auth()->user()->is_admin;
    }
}
