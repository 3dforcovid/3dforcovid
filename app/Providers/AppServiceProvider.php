<?php

namespace App\Providers;

use App\Models\File;
use App\Models\ShippingService;
use App\Models\SocialMediaService;
use App\Observers\FileObserver;
use App\Observers\ShippingServiceObserver;
use App\Observers\SocialMediaServiceObserver;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //

        // Register observers
        SocialMediaService::observe(SocialMediaServiceObserver::class);
        ShippingService::observe(ShippingServiceObserver::class);
        File::observe(FileObserver::class);
    }
}
