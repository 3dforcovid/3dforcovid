<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Package;
use Illuminate\Http\Request;
use Sauladam\ShipmentTracker\ShipmentTracker;

class ShippingServiceController extends Controller
{
    public function getStatus(Package $package)
    {
        $tracker = ShipmentTracker::get($package->shipping_service_name);

        if(!$tracker)
        {
            return response()->json(['message' => 'missing/ bad service', 'status' => false]);
        }

        try{
            $track = $tracker->track($package->tracking_number);

            return response()->json(['message' => $track, 'status' => true]);
        }catch(\Exception $e){
            // TODO :: Catch errors
        }

        return response()->json(['message' => 'missing/ bad tracking number', 'status' => false]);
    }
}
