<?php

namespace App\Http\Controllers\Authed;

use App\Http\Controllers\Controller;
use App\Jobs\ImageUploadJob;
use App\Models\File;
use App\Models\Package;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class PackageFileController extends Controller
{
    public function __construct()
    {
        $this->authorizeResource(Package::class, 'package');
    }

    /**
     * Display a listing of the resource.
     *
     * @param Package $package
     * @return Response
     */
    public function index(Package $package)
    {
        $files = $package->files;

        return view('packages.files.index', compact('package', 'files'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @param Package $package
     * @return Response
     */
    public function store(Request $request, Package $package)
    {
        $request_image = $request->file('package_image');
        $image_name    = "{$package->id}/package_image";

        ImageUploadJob::dispatchNow($request_image, $image_name, $package);

        session()->flash('saved');

        return redirect()->route('authed.packages.file.index', $package);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Package $package
     * @param File    $file
     * @return Response
     */
    public function destroy(Package $package, File $file)
    {
        $file->forceDelete();

        session()->flash('saved');

        return redirect()->route('authed.packages.file.index', $package);
    }
}
