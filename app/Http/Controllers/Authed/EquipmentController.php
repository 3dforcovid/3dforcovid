<?php

namespace App\Http\Controllers\Authed;

use App\Http\Controllers\Controller;
use App\Models\Dropdown;
use App\Models\Equipment;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\View\View;
use Str;

class EquipmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $equipment = auth()->user()->equipment;

        return view('equipment.index', compact('equipment'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Factory|View
     */
    public function create()
    {
        return view('equipment.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return RedirectResponse
     */
    public function store(Request $request)
    {
        $data = $request->except('_token');

        if ($request->get('brand_other')) {
            $option = Dropdown::whereSlug('brand')->first()->options()->firstOrCreate(['slug' => Str::slug($request->get('brand_other'))], [
                'label' => $request->get('brand_other'),
            ]);

            $data['brand_id'] = $option->id;
        }

        if ($request->get('model_other')) {
            $option = Dropdown::whereSlug('model')->first()->options()->firstOrCreate(['slug' => Str::slug($request->get('model_other'))], [
                'label'     => $request->get('model_other'),
                'parent_id' => $data['brand_id'],
            ]);

            $data['model_id'] = $option->id;
        }

        auth()->user()->equipment()->create($data);

        session()->flash('saved');

        return redirect()->route('authed.equipment.index');
    }

    /**
     * Display the specified resource.
     *
     * @param Equipment $equipment
     * @return Factory|View
     */
    public function show(Equipment $equipment)
    {
        return view('equipment.show', compact('equipment'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Equipment $equipment
     * @return Factory|View
     */
    public function edit(Equipment $equipment)
    {
        return view('equipment.edit', compact('equipment'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request   $request
     * @param Equipment $equipment
     * @return Response
     */
    public function update(Request $request, Equipment $equipment)
    {
        $data = $request->all();

        if ($request->get('brand_other')) {
            $option = Dropdown::whereSlug('brand')->first()->options()->firstOrCreate(['slug' => Str::slug($request->get('brand_other'))], [
                'label' => $request->get('brand_other'),
            ]);

            $data['brand_id'] = $option->id;
        }

        if ($request->get('model_other')) {
            $option = Dropdown::whereSlug('model')->first()->options()->firstOrCreate(['slug' => Str::slug($request->get('model_other'))], [
                'label'     => $request->get('model_other'),
                'parent_id' => $data['brand_id'],
            ]);

            $data['model_id'] = $option->id;
        }

        $equipment->update($data);

        session()->flash('saved');

        return redirect()->route('authed.equipment.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Equipment $equipment
     * @return Response
     */
    public function destroy(Equipment $equipment)
    {
        $equipment->delete();

        return redirect()->route('authed.equipment.index');
    }
}
