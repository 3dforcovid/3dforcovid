<?php

namespace App\Http\Controllers\Authed;

use App\Http\Controllers\Controller;
use App\Models\File;
use App\Models\Package;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class PackageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $packages = auth()->user()->packages;

        return view('packages.index', compact('packages'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('packages.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $this->authorize('create');

        auth()->user()->packages()->create($request->all());

        session()->flash('saved');

        return redirect()->route('authed.packages.index');
    }

    /**
     * Display the specified resource.
     *
     * @param Package $package
     * @return Response
     */
    public function show(Package $package)
    {
        return view('packages.show', compact('package'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Package $package
     * @return Response
     */
    public function edit(Package $package)
    {
        return view('packages.edit', compact('package'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param Package $package
     * @return Response
     */
    public function update(Request $request, Package $package)
    {
        $package->update($request->all());

        session()->flash('saved');

        return redirect()->route('authed.packages.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Package $package
     * @return Response
     */
    public function destroy(Package $package)
    {
        $package->delete();

        return redirect()->back();
    }
}
