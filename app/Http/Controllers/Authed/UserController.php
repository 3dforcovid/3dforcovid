<?php

namespace App\Http\Controllers\Authed;

use App\Http\Controllers\Controller;
use App\Http\Requests\UserRequest;
use App\Jobs\ImageUploadJob;
use App\Models\SocialMediaService;
use App\Models\User;
use Auth;

class UserController extends Controller
{
    public function profile()
    {
        $user = Auth::user();

        /** @var User $user */
        $social_media = $user->social_media()->with('service')->get()->keyBy('service.slug');

        return view('users.profile', compact('user', 'social_media'));
    }

    public function profile_save(UserRequest $request)
    {
        $request_image = $request->file('profile_image');
        $image_name    = auth()->id() . '/profile_image';

        if ($request_image) {
            ImageUploadJob::dispatchNow($request_image, $image_name, auth()->user());
        }

        /** @var User $user */
        $user = auth()->user();

        Auth::user()->update($request->except(['password', 'is_admin']));

        $services = SocialMediaService::all()->keyBy('slug');

        foreach ($request->get('social_media', []) as $service => $url) {
            if (!$url) {
                continue;
            }
            $user->social_media()->updateOrCreate(['social_media_service_id' => $services[$service]->id], [
                'url' => $url,
            ]);
        }

        if (($request->get('change_password') == '1') && $request->has('password')) {
            Auth::user()->update(['password' => bcrypt($request->get('password'))]);
        }

        session()->flash('saved');

        return redirect()->route('dashboard');
    }
}
