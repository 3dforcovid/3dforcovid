<?php

namespace App\Http\Controllers;

use App\Models\File;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class FilesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param File $files
     * @return Response
     */
    public function show(File $files)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param File $files
     * @return Response
     */
    public function edit(File $files)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param File    $files
     * @return Response
     */
    public function update(Request $request, File $files)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param File $files
     * @return Response
     */
    public function destroy(File $files)
    {
        //
    }
}
