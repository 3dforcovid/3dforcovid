<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\UserRequest;
use App\Models\User;
use Auth;
use Exception;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\View\View;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Factory|View
     */
    public function index()
    {
        $users = User::paginate(request('per_page', 15));

        return view('users.index', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     */
    public function create()
    {
        return view('users.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return RedirectResponse
     */
    public function store(Request $request)
    {
        $user = User::create($request->except('password'));

        if ($request->has('password')) {
            $user->update(['password' => bcrypt($request->get('password'))]);
        }

        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param User $user
     * @return Factory|View
     */
    public function show(User $user)
    {
        $user->loadMissing(['packages.service', 'social_media.service', 'equipment']);

        return view('users.show', compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param User $user
     * @return Factory|View
     */
    public function edit(User $user)
    {
        return view('users.edit', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param User    $user
     * @return RedirectResponse
     */
    public function update(Request $request, User $user)
    {
        $user->update($request->except('password'));

        if ($request->has('password')) {
            $user->update(['password' => bcrypt($request->get('password'))]);
        }

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param User $user
     * @return Response
     * @return RedirectResponse
     * @throws Exception
     */
    public function destroy(User $user)
    {
        $user->delete();

        return redirect()->back();
    }

    // Admin profile controller functions.
    public function profile()
    {
        $user = Auth::user();

        return view('users.profile', compact('user'));
    }

    public function profile_save(UserRequest $request)
    {
        Auth::user()->update($request->except(['password']));

        if ($request->has('password')) {
            Auth::user()->update(['password' => bcrypt($request->get('password'))]);
        }

        session()->flash('updated_message');

        return redirect()->back();
    }
}
