<?php

namespace App\Jobs;

use App\Models\Package;
use App\Models\User;
use Exception;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Http\UploadedFile;
use Illuminate\Queue\SerializesModels;
use Image;
use Storage;
use Str;

//use Illuminate\Bus\Queueable;
//use Illuminate\Contracts\Queue\ShouldQueue;
//use Illuminate\Queue\InteractsWithQueue;

class ImageUploadJob /*implements ShouldQueue*/
{
    use Dispatchable, /*InteractsWithQueue, Queueable,*/
        SerializesModels;
    /**
     * @var Image
     */
    private $image;
    /**
     * @var string
     */
    private $image_name;
    private $attach_to;

    /**
     * Create a new job instance.
     *
     * @param UploadedFile $image
     * @param string       $image_name
     * @param              $attach_to
     */
    public function __construct(UploadedFile $image, string $image_name, $attach_to)
    {
        $this->image      = $image;
        $this->image_name = $image_name;
        $this->attach_to  = $attach_to;
    }

    /**
     * Execute the job.
     *
     * @return void
     * @throws Exception
     */
    public function handle()
    {
        $type      = null;
        $disk      = null;
        $file_name = "{$this->image_name}.png";

        switch (true) {
            case $this->attach_to instanceof User:
                $type = 'user';
                $disk = 'profile_images';
                break;
            case $this->attach_to instanceof Package:
                $type = 'package';
                $disk = 'package_images';

                // for packages make sure that the name is unique to avoid overriding them
                $file_name = Str::of($file_name)
                                ->replaceLast('.png', '-')
                                ->append(Str::random(5))
                                ->append('.png');

                break;
            default:
                throw new Exception('Cannot attach the file');
                die();
        }

        // Process the image
        $image = Image::make($this->image)
                      ->orientate()
                      ->resize(400, 400, function ($constraint) {
                          $constraint->aspectRatio();
                      })
                      ->encode('png');

        // Save the image to disk
        Storage::disk($disk)->put(
            $file_name,
            $image
        );

        /**
         * store the image in the database
         */
        $md5_hash = md5($image . '');

        $default_attributes = [
            'file_name' => $file_name,
            'md5_sum'   => $md5_hash,
            'hash_name' => Str::uuid() . '',
            'meta_data' => [
                'original_name' => $this->image->getClientOriginalName(),
                'original_type' => $this->image->getClientOriginalExtension(),
            ],
        ];

        if ($type == 'user') {
            $this->attach_to->profile_image()->updateOrCreate(['is_profile' => true], array_merge($default_attributes, [
                'is_profile' => true,
            ]));
        } else {
            $this->attach_to->files()->create(array_merge($default_attributes, [
                'is_package' => true,
            ]));
        }
    }
}
