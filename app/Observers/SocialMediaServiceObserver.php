<?php

namespace App\Observers;

use App\Models\SocialMediaService;
use Str;

class SocialMediaServiceObserver
{
    /**
     * Handle the social media services "created" event.
     *
     * @param SocialMediaService $SocialMediaService
     * @return void
     */
    public function created(SocialMediaService $SocialMediaService)
    {
        if (!$SocialMediaService->slug) {
            $SocialMediaService->update(['slug' => Str::slug($SocialMediaService->label)]);
        }
    }

    /**
     * Handle the social media services "updated" event.
     *
     * @param SocialMediaService $SocialMediaService
     * @return void
     */
    public function updated(SocialMediaService $SocialMediaService)
    {
        //
    }

    /**
     * Handle the social media services "deleted" event.
     *
     * @param SocialMediaService $SocialMediaService
     * @return void
     */
    public function deleted(SocialMediaService $SocialMediaService)
    {
        //
    }

    /**
     * Handle the social media services "restored" event.
     *
     * @param SocialMediaService $SocialMediaService
     * @return void
     */
    public function restored(SocialMediaService $SocialMediaService)
    {
        //
    }

    /**
     * Handle the social media services "force deleted" event.
     *
     * @param SocialMediaService $SocialMediaService
     * @return void
     */
    public function forceDeleted(SocialMediaService $SocialMediaService)
    {
        //
    }
}
