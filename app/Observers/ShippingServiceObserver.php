<?php

namespace App\Observers;

use App\Models\ShippingService;
use Str;

class ShippingServiceObserver
{
    /**
     * Handle the shipping service "created" event.
     *
     * @param ShippingService $shippingService
     * @return void
     */
    public function created(ShippingService $shippingService)
    {
        if (!$shippingService->slug) {
            $shippingService->update(['slug' => Str::slug($shippingService->label)]);
        }
    }

    /**
     * Handle the shipping service "updated" event.
     *
     * @param ShippingService $shippingService
     * @return void
     */
    public function updated(ShippingService $shippingService)
    {
        //
    }

    /**
     * Handle the shipping service "deleted" event.
     *
     * @param ShippingService $shippingService
     * @return void
     */
    public function deleted(ShippingService $shippingService)
    {
        //
    }

    /**
     * Handle the shipping service "restored" event.
     *
     * @param ShippingService $shippingService
     * @return void
     */
    public function restored(ShippingService $shippingService)
    {
        //
    }

    /**
     * Handle the shipping service "force deleted" event.
     *
     * @param ShippingService $shippingService
     * @return void
     */
    public function forceDeleted(ShippingService $shippingService)
    {
        //
    }
}
