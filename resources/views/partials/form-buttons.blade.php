<div class="row">
    <div class="col-md-12">
        <button class="btn btn-primary" type="submit">
            <i class="fa fa-floppy-o"></i>
            Submit
        </button>

        @if($changePassword ?? false)
        <button type="button" class="btn btn-primary" id="changePassword">
            <i class="fa fa-pencil"></i>
            Change password
        </button>
        @endif

        <div class="float-md-right mt-3 mt-md-0">
            <a href="{{ url()->previous('/home') }}" class="btn btn-warning">
                <i class="fa fa-times"></i>
                Cancel
            </a>

            <button class="btn btn-danger" type="reset">
                <i class="fa fa-refresh"></i>
                Reset
            </button>
        </div>
    </div>
</div>
