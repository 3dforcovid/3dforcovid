@extends('layouts.app')


@section('content')
    <div class="container">
        <a href="{{route('authed.equipment.create')}}" class="btn btn-primary">Create</a>
        <hr>
        <table class="table table-hover table-stripped">
            <thead>
            <tr>
                <th>id</th>
                <th>Brand/ Model</th>
                <th>description</th>
                <th>created</th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            @forelse($equipment as $equip)
                <tr>
                    <td>
                        {{$equip->id}}
                    </td>
                    <td>
                        {{$equip->brand_name}} / {{$equip->model_name}}
                    </td>
                    <td>
                        {{Str::limit($equip->id)}}
                    </td>
                    <td>
                        {{$equip->created_at}}
                    </td>
                    <td>
                        <a href="{{route('authed.equipment.edit', $equip)}}" class="btn btn-primary">Edit</a>
                        <btn-confirm form_selector="#equipment-{{$equip->id}}-delete" label="Delete"></btn-confirm>
                        {!! Form::open(['id' => "equipment-{$equip->id}-delete", 'class' => 'hidden', 'route' => ['authed.equipment.destroy', $equip], 'method' => 'delete']) !!}
                        {!! Form::close() !!}
                    </td>
                </tr>
            @empty
                <tr>
                    <td colspan="100">
                        No Equipment
                    </td>
                </tr>
            @endforelse
            </tbody>
        </table>
    </div>
@endsection
