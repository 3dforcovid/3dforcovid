@extends('layouts.app')

@section('content')
    <div class="container">
        {!! Form::model($equipment, ['route' => ['authed.equipment.update', $equipment], 'method' => 'put']) !!}

        @include('equipment.fields')

        @include('partials.form-buttons')

        {!! Form::close() !!}
    </div>
@endsection
