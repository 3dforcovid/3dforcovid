@extends('layouts.app')

@section('content')
    <div class="container">
        {!! Form::open(['route' => 'authed.equipment.store', 'method' => 'post']) !!}

        @include('equipment.fields')

        @include('partials.form-buttons')

        {!! Form::close() !!}
    </div>
@endsection
