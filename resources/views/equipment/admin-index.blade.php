@extends('layouts.app')

@section('content')
    <div class="container">
        Total results: {{$equipment->total()}} <br>
        {!! $equipment->links() !!}

        <table class="table table-hover">
            <tr>
                <th>User</th>
                <th>Description</th>
                <th>Saved</th>
                <th>Brand / Model</th>
            </tr>
            @forelse($equipment as $item)
                <tr>
                    <td><a href="{{route('admin.users.show', $item->user_id)}}">{{$item->user_id}} - {{$item->user->name}}</a></td>
                    <td>{{Str::limit($item->AAA)}}</td>
                    <td>{{$item->updated_at}}</td>
                    <td>{{$item->brand_name}} / {{$item->model_name}}</td>
                </tr>
            @empty
                <tr>
                    <td colspan="100">
                        Nothing tracked
                    </td>
                </tr>
            @endforelse
        </table>
    </div>
@endsection
