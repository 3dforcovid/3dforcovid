@include('partials.form-errors')

<div class="row">
    <div class="col-md-12">
        <equipment
            old_brand="{{Form::getValueAttribute('brand_id')}}"
            old_model="{{Form::getValueAttribute('model_id')}}"
            :brand_options="{!! str_replace('"', "'", \App\Models\Dropdown::findBySlug('brand')->toJson()) !!}"
            :model_options="{!! str_replace('"', "'", \App\Models\Dropdown::findBySlug('model')->toJson()) !!}"
        ></equipment>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="form-group">
            <label for="description">Description</label>
            {!! Form::textarea('description', null, ['class' => 'form-control']) !!}
        </div>
    </div>
</div>
