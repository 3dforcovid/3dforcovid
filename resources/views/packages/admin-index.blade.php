@extends('layouts.app')


@section('content')
    <div class="container">
        Total results: {{$packages->total()}} <br>
        {!! $packages->links() !!}

        <table class="table table-hover table-stripped">
            <thead>
            <tr>
                <th>id</th>
                <th>user</th>
                <th>qty</th>
                <th>item</th>
                <th>description</th>
                <th>tracking</th>
                <th>last status</th>
                <th>Shipping Service</th>
                <th>shipped/ eta if known</th>
                <th>created</th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            @forelse($packages as $item)
                <tr>
                    <td>{{$item->id}}</td>
                    <td><a href="{{route('admin.users.show', $item->user_id)}}">{{$item->user_id}} - {{$item->user->name}}</a></td>
                    <td>{{$item->qty}}</td>
                    <td>{{$item->item}}</td>
                    <td>{{Str::limit($item->description)}}</td>
                    <td>{{$item->tracking_number}}</td>
                    <td>{{$item->last_status}}</td>
                    <td>{{$item->shipping_service_name}}</td>
                    <td>{{$item->ship_date}}@if($item->estimated_delivery_date) / @endif {{$item->estimated_delivery_date}}</td>
                    <td>{{$item->created_at}}</td>
                    <td>
                        <package-tracking package_id="{{$item->id}}"></package-tracking>
                    </td>
                </tr>
            @empty
                <tr>
                    <td colspan="100">
                        No Packages
                    </td>
                </tr>
            @endforelse
            </tbody>
        </table>
    </div>
@endsection
