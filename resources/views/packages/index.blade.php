@extends('layouts.app')


@section('content')
    <div class="container">
        <a href="{{route('authed.packages.create')}}" class="btn btn-primary">Create</a>
        <hr>
        <table class="table table-hover table-stripped">
            <thead>
            <tr>
                <th>id</th>
                <th>qty</th>
                <th>item</th>
                <th>description</th>
                <th>tracking</th>
                <th>last status</th>
                <th>Shipping Service</th>
                <th>shipped/ eta if known</th>
                <th>created</th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            @forelse($packages as $package)
                <tr>
                    <td>
                        {{$package->id}}
                    </td>
                    <td>
                        {{$package->qty}}
                    </td>
                    <td>
                        {{$package->item}}
                    </td>
                    <td>
                        {{Str::limit($package->description)}}
                    </td>
                    <td>
                        {{$package->tracking_number}}
                    </td>
                    <td>
                        {{$package->last_status}}
                    </td>
                    <td>
                        {{$package->shipping_service_name}}
                    </td>
                    <td>
                        {{$package->ship_date}}@if($package->estimated_delivery_date) / @endif {{$package->estimated_delivery_date}}
                    </td>
                    <td>
                        {{$package->created_at}}
                    </td>
                    <td>
                        <a href="{{route('authed.packages.file.index', $package)}}" class="btn btn-secondary">Files</a>
                        <a href="{{route('authed.packages.edit', $package)}}" class="btn btn-primary">Edit</a>
                        <btn-confirm form_selector="#package-{{$package->id}}-delete" label="Delete"></btn-confirm>
                        {!! Form::open(['id' => "package-{$package->id}-delete", 'class' => 'hidden', 'route' => ['authed.packages.destroy', $package], 'method' => 'delete']) !!}
                        {!! Form::close() !!}
                    </td>
                </tr>
            @empty
                <tr>
                    <td colspan="100">
                        No Packages
                    </td>
                </tr>
            @endforelse
            </tbody>
        </table>
    </div>
@endsection
