@extends('layouts.app')

@section('content')
    <div class="container">
        {!! Form::open(['class' => 'mb-5', 'files' => true, 'route' => ['authed.packages.file.store', $package], 'method' => 'post']) !!}
        <div class="form-group">
            <label for="package_image">Package image upload</label>
            <input type="file" class="form-control-file" name="package_image" id="package_image" placeholder="Package image upload" aria-describedby="package_image-help">
            <small id="package_image-help" class="form-text text-muted">One at a time please.</small>
        </div>
        <button class="btn btn-primary" type="submit">
            <i class="fa fa-floppy-o"></i>
            Submit
        </button>
        {!! Form::close() !!}

        <ul class="list-unstyled">
            @if($package->files->count())
            <li class="mb-5">
                <h3>
                    Please note: for these delete buttons there is no confirmation, and it is not reversible.
                </h3>
            </li>
            @endif
            @forelse($package->files as $file)
                <li>
                    {!! Form::open(['route' => ['authed.packages.file.destroy', $package, $file], 'method' => 'delete']) !!}
                    <button type="submit" class="btn btn-danger mr-3 mb-3">
                        <i class="fa fa-times"></i> Delete
                    </button>
                    <img src="{{route('file.show', ['package_images', $file->hash_name, 500])}}" alt="Shipment image"/>
                </li>
            @empty
                No files to show
            @endforelse
        </ul>
    </div>
@endsection
