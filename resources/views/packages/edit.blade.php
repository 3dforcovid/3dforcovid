@extends('layouts.app')

@section('content')
    <div class="container">
        {!! Form::model($package, ['route' => ['authed.packages.update', $package], 'method' => 'put']) !!}

        @include('packages.fields')

        @include('partials.form-buttons')

        {!! Form::close() !!}
    </div>
@endsection
