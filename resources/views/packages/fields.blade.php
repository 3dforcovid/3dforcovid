@include('partials.form-errors')

<div class="row">
    <div class="col-md-4">
        <div class="form-group">
            <label for="item">Item <small>example: 3d printer, laser thing, etc, etc</small></label>
            {!! Form::text('item', null, ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <label for="tracking_number">Tracking number</label>
            {!! Form::text('tracking_number', null, ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <label for="shipping_service_id">Shipping Service</label>
            {!! Form::select('shipping_service_id', \App\Models\ShippingService::all()->pluck('label', 'id')->prepend('Please Select', '0'), null, ['class' => 'form-control select2']) !!}
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-3">
        <div class="form-group">
            <label for="qty">Qty</label>
            {!! Form::number('qty', old('qty', 1), ['class' => 'form-control', 'step' => 1]) !!}
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group">
            <label for="ship_date">Shipped Date</label>
            {!! Form::text('ship_date', old('ship_date', new \Carbon\Carbon()), ['class' => 'form-control date-picker']) !!}
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group">
            <label for="estimated_delivery_date">Estimated Delivery Date</label>
            {!! Form::text('estimated_delivery_date', null, ['class' => 'form-control date-picker']) !!}
        </div>
    </div>
    <div class="col-md-3">
        <div class="form-group">
            <label for="last_status">Shipping Status</label>
            {!! Form::text('last_status', null, ['list' => 'shipping_statuses', 'class' => 'form-control']) !!}

            <datalist id="shipping_statuses">
                <option value="packaging">
                <option value="shipped">
                <option value="delivered">
                <option value="canceled">
            </datalist>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="form-group">
            <label for="description">Description</label>
            {!! Form::textarea('description', null, ['class' => 'form-control']) !!}
        </div>
    </div>
</div>
