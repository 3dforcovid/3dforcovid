@extends('layouts.app')

@section('content')
    <div class="container">
        {!! Form::open(['route' => 'authed.packages.store', 'method' => 'post']) !!}

        @include('packages.fields')

        @include('partials.form-buttons')

        {!! Form::close() !!}
    </div>
@endsection
