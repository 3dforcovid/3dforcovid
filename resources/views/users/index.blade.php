@extends('layouts.app')


@section('content')
    <div class="container">

        Total results: {{$users->total()}} <br>
        {!! $users->links() !!}

        <table class="table table-hover table-stripped">
            <thead>
            <tr>
                <th></th>
                <th>id</th>
                <th>name</th>
                <th>email</th>
                <th>created</th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            @forelse($users as $user)
                <tr>
                    <td><img src="{{$user->sm_profile_image_url}}" alt="Profile image" class="rounded-circle"/></td>
                    <td>{{$user->id}}</td>
                    <td>{{$user->name}}</td>
                    <td>{{$user->email}}</td>
                    <td>{{$user->created_at}}</td>
                    <td>
                        <a href="{{route('admin.users.edit', $user)}}" class="btn btn-primary">Edit</a>
                        <a href="{{route('admin.users.show', $user)}}" class="btn btn-secondary">Show</a>
                    </td>
                </tr>
            @empty
                <tr>
                    <td colspan="100">
                        No Users
                    </td>
                </tr>
            @endforelse
            </tbody>
        </table>
    </div>
@endsection
