@include('partials.form-errors')

<div class="row">
    <div class="col-md-12">
        <div class="form-group">
            <label for="is_admin" class="text-danger">
                {!! Form::checkbox('is_admin') !!} Is Admin
            </label>
        </div>
    </div>
</div>

<hr>

<div class="row">
    <div class="col-md-6">

        <div class="form-group">
            <label for="first_name">First Name</label>
            {!! Form::text('first_name', null, ['class' => 'form-control']) !!}
            @error('first_name')
            <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
            @enderror
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label for="last_name">Last Name</label>
            {!! Form::text('last_name', null, ['class' => 'form-control']) !!}
            @error('last_name')
            <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
            @enderror
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="form-group">
            <label for="email">Email</label>
            {!! Form::email('email', null, ['class' => 'form-control']) !!}
            @error('email')
            <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
            @enderror
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <label for="password">Password</label>
            {!! Form::password('password', ['class' => 'form-control']) !!}
            @error('password')
            <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
            @enderror
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <label for="first_name">Password confirm</label>
            {!! Form::password('password_confirmation', ['class' => 'form-control']) !!}
            @error('password_confirmation')
            <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
            @enderror
        </div>
    </div>
</div>
