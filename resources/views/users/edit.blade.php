@extends('layouts.app')

@section('content')
    <div class="container">

        {!! Form::model($user, ['route' => ['admin.users.update', $user], 'method'=> 'put']) !!}

        @include('users.fields')

        @include('partials.form-buttons')

        {!! Form::close() !!}

    </div>

@endsection
