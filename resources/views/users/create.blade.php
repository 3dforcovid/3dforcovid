@extends('layouts.app')

@section('content')
    <div class="container">

        {!! Form::open(['route' => ['admin.user.store'], 'method'=> 'post']) !!}

        @include('users.fields')

        @include('partials.form-buttons')

        {!! Form::close() !!}

    </div>

@endsection
