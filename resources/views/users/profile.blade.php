@extends('layouts.app')

@section('content')
    <div class="container">

        {!! Form::model($user, ['route' => ['authed.user.profile.save'], 'method'=> 'put', 'files' => true]) !!}

        @include('partials.form-errors')

        <div class="row">
            <div class="offset-md-6 col-md-6">
                @if($user->profile_image_url)
                <img src="{{$user->profile_image_url}}" alt="Profile image" />
                @endif

                <div class="form-group">
                    <label for="profile_image">Profile image</label>
                    <input type="file" class="form-control-file" name="profile_image" id="profile_image" placeholder="Profile image" aria-describedby="profile_image_help">
                    <small id="profile_image_help" class="form-text text-muted">Profile image</small>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">

                <div class="form-group">
                    <label for="first_name">First Name</label>
                    {!! Form::text('first_name', null, ['class' => 'form-control']) !!}
                    @error('first_name')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="last_name">Last Name</label>
                    {!! Form::text('last_name', null, ['class' => 'form-control']) !!}
                    @error('last_name')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label for="email">Email</label>
                    {!! Form::email('email', null, ['class' => 'form-control']) !!}
                    @error('email')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
            </div>
        </div>

        @if(\App\Models\SocialMediaService::count())
            <section id="social-media">
                <div class="row">

                    @foreach(\App\Models\SocialMediaService::all() as $service)
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="{{$service->slug}}">{{$service->label}}</label>
                                {!! Form::text("social_media[{$service->slug}]", \Illuminate\Support\Arr::get($social_media, "{$service->slug}.url", ''), ['id' => $service->slug, 'class' => 'form-control']) !!}
                            </div>
                        </div>
                    @endforeach
                </div>
            </section>
        @endif

        <div class="row" style="display: none;" id="password-section">
            <input type="hidden" name="change_password" value="0"/>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="password">Password</label>
                    {!! Form::password('password', ['class' => 'form-control']) !!}
                    @error('password')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="first_name">Password confirm</label>
                    {!! Form::password('password_confirmation', ['class' => 'form-control']) !!}
                    @error('password_confirmation')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
            </div>
        </div>

        @include('partials.form-buttons', ['changePassword' => true])

        {!! Form::close() !!}

    </div>

@endsection

@section('js')
    <script type="text/javascript">
        $(function () {
            $("#changePassword").click(function(){
                $("#password-section").fadeToggle('fast');

                const element = $("[name='change_password']");

                element.val(
                    element.val() != '1'? '1': '0'
                )
            })
        });
    </script>
@endsection
