@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div>
                    <img src="{{$user->sm_profile_image_url}}" alt="Profile image" class="rounded-circle"/>
                </div>
                <div>Name: {{$user->name}}</div>
                <div>Email: {{$user->email}}</div>
                <div>Saved: {{$user->updated_at}}</div>
            </div>
            <div class="col-md-6">

                <table class="table table-hover">
                    <tbody>
                    @forelse($user->social_media as $item)
                        <tr>
                            <td>
                                <strong>{{$item->name}}</strong> - {{$item->url}}
                            </td>
                        </tr>
                    @empty
                        <tr>
                            <td>No social media entered</td>
                        </tr>
                    @endforelse
                    </tbody>
                </table>

            </div>
        </div>

        <div class="row mt-5">
            <div class="col-md-6">
                <h3>Equipment</h3>
                <hr>
                <table class="table table-hover">
                    <thead>
                    <tr>
                        <th>Brand / Model</th>
                    </tr>
                    </thead>
                    <tbody>
                    @forelse($user->equipment as $item)
                        <tr>
                            <td>
                                {{$item->brand_name}} / {{$item->model_name}}
                            </td>
                        </tr>

                    @empty
                        <tr>
                            <td>Nothing logged</td>
                        </tr>
                    @endforelse
                    </tbody>
                </table>
            </div>
            <div class="col-md-6">
                <h3>Packages</h3>
                <hr>
                <div class="table-responsive">
                    <table class="table table-hover">
                        <thead>
                        <tr>
                            <th>Item</th>
                            <th>Description</th>
                            <th>QTY</th>
                            <th>Shipping Service</th>
                            <th>Tracking number</th>
                            <th>Shipped Date</th>
                            <th>Es. Delivery Date</th>
                            <th>Delivered Date</th>
                            <th>Last Status</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            @forelse($user->packages as $item)
                                <td>{{$item->item}}</td>
                                <td>{{Str::limit($item->description)}}</td>
                                <td>{{$item->qty}}</td>
                                <td>{{$item->shipping_service_name}}</td>
                                <td>{{$item->tracking_number}}</td>
                                <td>{{$item->ship_date}}</td>
                                <td>{{$item->delivered_date}}</td>
                                <td>{{$item->estimated_delivery_date}}</td>
                                <td>{{$item->last_status}}</td>
                            @empty
                                <td colspan="100">Nothing logged</td>
                            @endforelse
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
