![3dforcovid](public/images/logo.png)

## Start

### install node and laravel packages
run `yarn 3d:install`

### Create your database. Sample command here.
```bash
mysql -u root -p -e"create database 3dforcovid"
```

### Set your database credentials

`cp .env.example .env` 

edit the .env and set your database credentials

### Generate the project key

`php artisan key:generate`

### migrate the database

`php artisan migrate --seed`

to start developing you can run 
- Without queue listener - `3d:dev`
- With queue listener - `3d:dev-enable-queue`

### should be good to start the server
`yarn 3d:dev`

after running `yarn 3d:dev` it should open your browser and watch for
 changes and auto reload the browser when it sees the changes.
 
browser sync handles that, keeping your browser tab in sync and reloading when it sees a change.

### you can monitor the server with 
`yarn 3d:monit`

### Dev notes

**View more yarn scripts in the package.json scripts section**

For some reason when using browsersync the debug bar does not show up. if you need it just use the project url
http://localhost:8061/

### Shipment tracking 
* package - https://github.com/sauladam/shipment-tracker
  
#### Supported Carriers
The following carriers and languages are currently supported by this package:

* DHL (de, en)
* DHL Express (de, en) (so far only for waybill numbers, not for shipment numbers of the individual pieces)
* GLS (de, en)
* UPS (de, en)
* Fedex (en) (hat tip to @avrahamappel)
* USPS (en)
* PostCH (Swiss Post Service) (de, en)
* PostAT (Austria Post Service) (de, en)

### Laravel queue

By default laravel sets the queue manager to array but if that is changed
I implemented pm2 to run the laravel queue processor. That can be started with `yarn 3d:queue`

you can see all the running pm2 jobs with `pm2 l` and you can delete them all if you want with `pm2 delete all` 
 
See the pm2 link below for more information

## Notes

* This project uses laravel and vue - https://laravel.com/
* use yarn - https://yarnpkg.com/
* pm2 - https://pm2.keymetrics.io/
* image processing - http://image.intervention.io/use/basics

## About Laravel

Laravel is a web application framework with expressive, elegant syntax. We believe development must be an enjoyable and creative experience to be truly fulfilling. Laravel takes the pain out of development by easing common tasks used in many web projects, such as:

- [Simple, fast routing engine](https://laravel.com/docs/routing).
- [Powerful dependency injection container](https://laravel.com/docs/container).
- Multiple back-ends for [session](https://laravel.com/docs/session) and [cache](https://laravel.com/docs/cache) storage.
- Expressive, intuitive [database ORM](https://laravel.com/docs/eloquent).
- Database agnostic [schema migrations](https://laravel.com/docs/migrations).
- [Robust background job processing](https://laravel.com/docs/queues).
- [Real-time event broadcasting](https://laravel.com/docs/broadcasting).

Laravel is accessible, powerful, and provides tools required for large, robust applications.

## Learning Laravel

Laravel has the most extensive and thorough [documentation](https://laravel.com/docs) and video tutorial library of all modern web application frameworks, making it a breeze to get started with the framework.

If you don't feel like reading, [Laracasts](https://laracasts.com) can help. Laracasts contains over 1500 video tutorials on a range of topics including Laravel, modern PHP, unit testing, and JavaScript. Boost your skills by digging into our comprehensive video library.
