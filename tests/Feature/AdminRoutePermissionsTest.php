<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class AdminRoutePermissionsTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testGuestUserList()
    {
        $this->assertGuest();

        $response = $this->get(route('admin.users.index'));

        $response->assertRedirect(route('login'));
    }
}
