<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('user/profile', 'UserController@profile')->name('user.profile');
Route::put('user/profile', 'UserController@profile_save')->name('user.profile.save');

Route::resource('packages', 'PackageController');
Route::resource('packages.file', 'PackageFileController')->except(['show', 'edit', 'update']);
Route::resource('equipment', 'EquipmentController');

//Route::delete('packages/files/{file}', 'Authed\PackageController@destroy_file')->name('packages.image.delete');
