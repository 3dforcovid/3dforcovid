<?php

use Illuminate\Support\Facades\Route;
use Intervention\Image\Constraint;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/home', 'HomeController@index')->name('dashboard');

/**
 * Handle serving up images in a standard way
 */
Route::get('/files/{storage}/{hash_name}/{width?}', function ($storage, $hash_name, $width = 200) {
    // Using cache here to service up a cached version of the image based on hash
    // is safe because the hash is unique and gets changed out when a new image is uploaded
    return Cache::remember("{$hash_name}/{$width}", 1, function () use ($storage, $hash_name, $width) {
        $file      = \App\Models\File::where('hash_name', $hash_name)->first();
        $file_path = Arr::get($file, 'file_name');

        // if the image does not exist default it to something else
        if (!$file || !Storage::disk($storage)->exists($file_path)) {
            $file_path = 'placeholder.png';
        }

        return Image::make(Storage::disk($storage)->get($file_path))
                    ->resize($width, $width, function (Constraint $constraint) {
                        $constraint->aspectRatio();
                    })
                    ->response();
    });
})->name('file.show');
