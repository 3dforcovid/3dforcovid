<?php

/** @var Factory $factory */

use App\Models\SocialMediaService;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;

$factory->define(SocialMediaService::class, function (Faker $faker) {
    $name = $faker->name;

    return [
        'slug'  => Str::slug($name),
        'label' => $name,
        'url'   => $faker->url,
    ];
});
