<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePackagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('packages', function (Blueprint $table) {
            $table->id();

            $table->string('item');
            $table->string('description')->nullable();
            $table->double('qty');

            $table->unsignedBigInteger('user_id')->index();
            $table->unsignedBigInteger('shipping_service_id')->index();

            $table->string('tracking_number')->nullable();

            //when was this package shipped
            $table->dateTime('ship_date')->nullable();
            // expected delivery date
            $table->dateTime('estimated_delivery_date')->nullable();
            $table->dateTime('delivered_date')->nullable();

            $table->string('last_status')->nullable();

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('packages');
    }
}
