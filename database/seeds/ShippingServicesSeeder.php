<?php

use App\Models\ShippingService;
use Illuminate\Database\Seeder;

class ShippingServicesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $list = [
            ['label' => 'DHL'],
            ['label' => 'DHL Express'],
            ['label' => 'GLS'],
            ['label' => 'UPS'],
            ['label' => 'Fedex'],
            ['label' => 'USPS'],
            ['label' => 'PostCH'],
            ['label' => 'PostAT'],
        ];

        foreach ($list as $item) {
            ShippingService::firstOrCreate(['slug' => Str::slug($item['label'])], $item);
        }
    }
}
