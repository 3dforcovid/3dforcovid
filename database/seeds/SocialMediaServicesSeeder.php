<?php

use App\Models\SocialMediaService;
use Illuminate\Database\Seeder;

class SocialMediaServicesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $list = [
            ['label' => 'Facebook'],
            ['label' => 'Twitter'],
            ['label' => 'LinkedIn'],
            ['label' => 'Instagram'],
            ['label' => 'Pinterest'],
        ];

        foreach ($list as $item) {
            SocialMediaService::firstOrCreate(['slug' => Str::slug($item['label'])], $item);
        }
    }
}
