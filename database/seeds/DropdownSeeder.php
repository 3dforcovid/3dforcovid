<?php

use App\Models\Dropdown;
use Illuminate\Database\Seeder;

class DropdownSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $brand = Dropdown::firstOrCreate(['slug' => 'brand'], [
            'label' => 'Brand',
            'type'  => 'type',
        ]);

        $cr = $brand->options()->create([
            'label' => 'Creativity',
        ]);

        $bo = $brand->options()->create([
            'label' => 'BossLaser',
        ]);

        $models = Dropdown::firstOrCreate(['slug' => 'model'], [
            'label' => 'Model',
            'type'  => 'model',
        ]);

        $models->options()->create([
            'label' => '3D Printer - CR-010',
            'parent_id' => $cr->id,
        ]);

        $models->options()->create([
            'label' => 'Laser - BOSS LS-1416',
            'parent_id' => $bo->id,
        ]);
    }
}
