<?php

use App\Models\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = User::firstOrCreate(['email' => 'admin@admin.com'], [
            'is_admin'   => true,
            'first_name' => 'admin',
            'last_name'  => 'admin',
            'email'      => 'admin@admin.com',
            'password'   => bcrypt('admin'),
        ]);

        $maker = User::firstOrCreate(['email' => 'make@make.com'], [
            'first_name' => 'make',
            'last_name'  => 'make',
            'email'      => 'make@make.com',
            'password'   => bcrypt('make'),
        ]);
    }
}
